# High Availability ElasticSearch

This is an ElasticSearch project with the following characteristics:

* Self-Healing
  * Self-Provisioning - every system can bootstrap itself and install the necessary configuration
  * Auto-Scaling - facility for scale up/down also acts as disaster recovery, creating n number of systems 
* Automatic Discovery - every node in the automatically discovers each other.
